import React from 'react';
import Button from '../Button/Button';
import { useTelegram } from '../../hooks/useTelegram';
import './header.css'
import { Link } from "react-router-dom";

const Header = () => {
  const {tg,onClose,user,}= useTelegram();

  return (
    <div className='header'>
      <Button title="Close!"
        onClick={onClose}
      />
      <Link to="/form">Forma</Link>
      <Link to="/">Main</Link>
      <span className='username'>
       user: {user?.username}
      </span>
    </div>
  );
};

export default Header;