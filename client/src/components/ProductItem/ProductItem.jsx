import React from 'react';
import './productItem.css';
import Button from '../Button/Button';

const ProductItem = ({product, className, onAdd, key}) => {

  const onAddHandler = () => {
    onAdd(product);
}
  return (
    <div className={'product ' + className} key={key}>
            <div className={'img'}> <img src={product.img}/> </div>
            <div className={'title'}>{product.title}</div>
            <div className={'description'}>{product.description}</div>
            <div className={'price'}>
                <span>Стоимость: <b>{product.price} UAH</b></span>
            </div>
            <Button className={'add-btn'} onClick={onAddHandler} title='Добавить в корзину'/>
               
            
        </div>
  );
};

export default ProductItem;