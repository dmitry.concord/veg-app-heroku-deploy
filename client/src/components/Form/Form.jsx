//import TelegramBot from 'node-telegram-bot-api';
import React, { useCallback, useEffect, useState } from 'react';
import { useNavigate, useLocation,  } from 'react-router-dom';
import { useTelegram } from '../../hooks/useTelegram';
import './form.css';
import { collection, addDoc } from "firebase/firestore"; 
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { firebaseConfig } from '../../firebase';
import Button from '../Button/Button';

const Form = ({location}) => {
  const { tg } = useTelegram();
  //const location = useLocation();
  //console.log(location)
  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app); 


  const [city, setCity] = useState('');
  const [street,setStreet] = useState('');
  const [payment, setPayment] =useState('');

  const onSendData = useCallback(async()=>{
    const data ={
      'city':city,
      'street':street,
      'payment':payment ?payment : 'cash',
      'time': Date.now()

    }
 try{
const docRef = await addDoc(collection(db,'adress'),data);
console.log("Document written with ID: ", docRef);
}
catch(e){
  console.error("Error adding document: ", e);
} 
   
    tg.sendData(JSON.stringify(data));
  },[city, street, payment,])

  useEffect(()=>{
    tg.onEvent('mainButtonClicked', onSendData)
    return ()=> tg.offEvent('mainButtonClicked', onSendData)
    
  },[onSendData])

  useEffect(()=>{
  tg.MainButton.setParams({
  text:'Send data'
})
  },[])

  useEffect(()=>{
   if(!city||!street){
    tg.MainButton.hide();

   }  else {
    tg.MainButton.show();
   } 
      },[street,city,])

      

 

  const onChangeCity =(e)=> {
    setCity(e.target.value)
  }

  const onChangeStreet =(e)=> {
    setStreet(e.target.value)
  }
  const onChangePayment =(e)=> {
    setPayment(e.target.value)
  }
  return (
    <div className='form'>
    <h3>Enter your name</h3>
    <input type="text" placeholder='City' onChange={onChangeCity} className='input' value={city}/>
    <input type="text" placeholder='Street' onChange={onChangeStreet} className='input' value={street}/>
    <label htmlFor="select">Payment:</label>
    <select className={'select'} onChange={onChangePayment} value={payment}>
    <option value="online">Credit Card</option>
    <option value="ofline">Cash</option>

    </select>
    {/* <Button onClick={onSendData} title='Send data'/> */}
      FORMA
    </div>
  );
};

export default Form;