import React, {useCallback, useEffect, useState } from 'react';
import ProductItem from '../ProductItem/ProductItem';
import './productList.css';
import { initializeApp } from "firebase/app";

import { firebaseConfig, } from '../../firebase';
import { collection, query, getDocs, getFirestore, addDoc } from "firebase/firestore";
import Loader from '../Loader/Loader'; 
import { useTelegram } from '../../hooks/useTelegram';
import { useNavigate } from 'react-router-dom';
 
const getTotalPrice = (items = [])=>{
return items.reduce((acc,item)=>{
return acc+= Number(item.price)
},0)
}

const ProductList = ({location}) => {
  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app);
  
  const {tg, queryId} = useTelegram();
  const [products, setProducts] = useState([]);
  const [error, setError]= useState([]);
  const [load, setLoad] = useState(true);
  const [addedItems, setAddedItems] = useState([])

  const navigate = useNavigate()

  const onAdd = (product)=> {
    const alreadyAdded = addedItems.find(item => item.id === product.id);
    let newItems =[];
    if(alreadyAdded){
      newItems = addedItems.filter(item => item.id !== product.id);
    }else {
      newItems = [...addedItems,product]
    }
    setAddedItems(newItems)
    if(newItems.length ===0){
      tg.MainButton.hide()
    }else {
      tg.MainButton.show()
      tg.MainButton.setParams({
      text: `Buy ${getTotalPrice(newItems) +' UAH'}`
      })
    }
  }
/* 
  const onSendData = useCallback(async()=>{
    const data ={
      products: addedItems,
      totalPrice: getTotalPrice(addedItems),
    }
 try{
  const docRef = await addDoc(collection(db,'orders'),data);
  console.log("Document written with ID: ", docRef);
}
catch(e){
  console.error("Error adding document: ", e);
} 
    
    tg.sendData(JSON.stringify(data));
    tg.openLink(`${location}` + '/form')
    navigate(`${location}` + '/form');
    //console.log(`${location}` + '/form')
  },[addedItems]) */
  
  const onSendData = useCallback(async()=>{
    const data ={
      products: addedItems,
      totalPrice: getTotalPrice(addedItems),
    }
     try{
    const docRef = await addDoc(collection(db,'orders'), data);
    console.log("Document written with ID: ", docRef);
    }
     catch(e){
  console.error("Error adding document: ", e);
} 
    
    navigate('/form');

  },[addedItems])

  useEffect(()=>{
    tg.onEvent('mainButtonClicked', onSendData)
    return ()=> tg.offEvent('mainButtonClicked', onSendData)
   
  },[onSendData])


    const arr1 = [];
    async function readASingleDocument(){
    const querySnapshot = await getDocs(collection(db, "products"))
      querySnapshot.forEach((doc) => {
     
      const element = arr1.some((elem)=>(doc.data().id == elem.id))
      //console.log(element) 
     if(!element){
      arr1.push(doc.data())
     }else{
      return null
     }
    })
   setProducts(arr1)
   setLoad(false)
  } 

 


 useEffect(()=>{
 const prod = readASingleDocument()

},[])
 
  return (
    <div className={'list'}>

{!products&&load? <Loader/> :products.map(item=> (<ProductItem product={item}  className={'item'} key={item.id} onAdd={onAdd}/>)) }


    </div>
  );
};

export default ProductList;