import React, { useEffect} from 'react';
import {Route, Routes} from 'react-router-dom';
import './App.css';
import { useTelegram } from './hooks/useTelegram';
import Header from './components/Header/Header';
import Button from './components/Button/Button';
import ProductList from './components/ProductList/ProductList';
import Form from './components/Form/Form';
/*  import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";  */

function App() {
 // const tg = window.Telegram.WebApp
 /*  const app = initializeApp(firebaseConfig);
 const db = getFirestore(app);  */
 //console.log(window.location.href)
 const location = window.location.href;
 const {tg,onClose, onToggleButton}= useTelegram();

  useEffect(()=>{
    tg.ready()
    console.log(tg)
  },[])
  
  return (
    <div className="App">
    <Header/>
      Work!!
      <Routes>
        <Route index element={<ProductList/>}/>
        <Route path={'/form'} element={<Form location={location}/>}/>
      </Routes>
      <Button title={'toggle!'}
        onClick={onToggleButton}
      />
     
    </div>
  );
}

export default App;
